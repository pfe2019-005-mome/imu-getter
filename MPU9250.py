import socketio
import sys
import asyncio
import RTIMU
import math
import time


if len(sys.argv) != 4:
    print("Program need only three arguments")
    sys.exit(1)

file = sys.argv[1]
articulation = sys.argv[2]

settings = RTIMU.Settings(file)
imu = RTIMU.RTIMU(settings)

if not imu.IMUInit():
    print("Imu init failed")
    sys.exit(1)
else:
    print("Imu init success")

imu.setSlerpPower(0.02)
imu.setGyroEnable(True)
imu.setAccelEnable(True)
imu.setCompassEnable(True)

poll_interval = imu.IMUGetPollInterval()
print("Poll interval in ms: ", poll_interval)

play = False

rec = False
store = []

sio = socketio.Client()

def getMillis():
    return int(round(time.time() * 1000))

async def startRecord(seconds):
    global store, rec
    store = []
    goalTime = seconds * 1000
    start = getMillis()
    now = start
    rec = True
    while (now - start) < goalTime:
        now = getMillis()
    rec = False
    sio.emit("new-record", {"imu": file, "store": store})

def storeImuData(data):
    global store
    store.push[data]

def retrieveData(data):
    fusion = data["fusionPose"]
    fusionQ = data["fusionQPose"]
    gyro = data["gyro"]
    accel = data["accel"]
    compass = data["compass"]
    return {
        "timestamp": data["timestamp"],
        "rate": data["rate"],
        "fusionPose": {"x": fusion[0], "y": fusion[1], "z": fusion[2]},
        "fusionQPose": {"scalar": fusionQ[0], "x": fusionQ[1], "y": fusionQ[2], "z": fusionQ[3]},
        "gyro": {"x": gyro[0], "y": gyro[1], "z": gyro[2]},
        "accel": {"x": accel[0], "y": accel[1], "z": accel[2]},
        "compass": {"x": compass[0], "y": compass[1], "z": compass[2]}
    } 

async def startIMURead():
    rate = 0
    lastrate = 0
    last = getMillis()
    now = getMillis()
    while play:
        while imu.IMURead():
            if (now-last) > 1000 :
                last = now
                lastrate = rate
                rate = 0
            data = imu.getIMUData()
            data["rate"] = lastrate
            collect = retrieveData(data)
            if rec: storeImuData(collect)
            rate = rate + 1
            now = getMillis()
            sio.emit(articulation, collect)
        sio.sleep(poll_interval*1.0/1000.0)

@sio.event
def connect():
    print("Connected!")
    launch()

@sio.event
def connect_error():
    print("The connection failed!")

@sio.event
def disconnect():
    print("Disconnected!")

@sio.event
def launch():
    global play
    if play: return
    print("Launched")
    play=True
    asyncio.run(startIMURead())

@sio.event
def stop():
    global play
    print("Stoped")
    play = False

@sio.event
def record(seconds):
    asyncio.run(startRecord(seconds))


sio.connect(sys.argv[3])

